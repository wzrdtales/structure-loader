"use strict";

const path = require("path");
const Promise = require("bluebird");
const glob = Promise.promisify(require("glob"));

module.exports = function(options = {}) {
  return glob(options.globPath || path.resolve("./lib/job/**/*.js"))
    .map(service => require(service))
    .reduce((obj, service) => {
      obj[service.name] = service.handler;
      return obj;
    }, {});
};
